package br.ucsal.testequalidade.restaurante.domain;

import java.util.Objects;

import br.ucsal.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class Mesa {

	private SituacaoMesaEnum situacao = SituacaoMesaEnum.LIVRE;

	private Integer numero;

	private Integer capacidade;

	public SituacaoMesaEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoMesaEnum situacao) {
		this.situacao = situacao;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
	}

	@Override
	public int hashCode() {
		return Objects.hash(capacidade, numero, situacao);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mesa other = (Mesa) obj;
		return Objects.equals(capacidade, other.capacidade) && Objects.equals(numero, other.numero)
				&& situacao == other.situacao;
	}

	@Override
	public String toString() {
		return "Mesa [situacao=" + situacao + ", numero=" + numero + ", capacidade=" + capacidade + "]";
	}


}
